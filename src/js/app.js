'use strict'

const menuBtn = document.querySelector('.menu-burger-btn');
const menuActive = document.querySelector('.menu-burger-btn').children;
const menuList = document.querySelector('.menu-burger-list');

menuBtn.addEventListener("click", function () {
    for (let element of menuActive) {
        element.classList.toggle("active");
    }
    menuList.classList.toggle("active");
})




